FROM sunng/alpine-jlink-base

ADD target/default/jlink /opt/cassanbench
ENTRYPOINT /opt/cassanbench/bin/cassanbench
