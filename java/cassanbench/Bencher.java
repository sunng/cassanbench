package cassanbench;

import com.datastax.driver.core.Cluster;
import com.datastax.driver.core.PoolingOptions;
import com.datastax.driver.core.HostDistance;
import com.datastax.driver.core.Session;
import com.datastax.driver.core.SimpleStatement;
import com.datastax.driver.core.ConsistencyLevel;

public final class Bencher {

    public static Cluster cluster;

    public static Session session;

    public static final void init() {
        PoolingOptions poolingOptions = new PoolingOptions();
        poolingOptions
            .setCoreConnectionsPerHost(HostDistance.LOCAL,  4)
            .setMaxConnectionsPerHost(HostDistance.LOCAL, 10);

        cluster = Cluster.builder()
            .addContactPoint(System.getenv("CASSANDRA_HOST"))
            .withPoolingOptions(poolingOptions)
            .build();

        session = cluster.connect("laughing_fish");
    }

    private static final void _write(String uuid, ConsistencyLevel level) {
        SimpleStatement stmt = new SimpleStatement("INSERT INTO user (first_name, last_name) VALUES (?, ?)", uuid, uuid);
        stmt.setConsistencyLevel(level);
        session.execute(stmt);
    }

    public static final void write(String uuid) {
        _write(uuid, getConfiguredConsistencyLevel());
    }

    public static final void writeAll(String uuid) {
        _write(uuid, ConsistencyLevel.ALL);
    }

    public static final void writeOne(String uuid) {
        _write(uuid, ConsistencyLevel.ONE);
    }

    public static final void writeAny(String uuid) {
        _write(uuid, ConsistencyLevel.ANY);
    }

    /**
     * read consistent level from env
     */
    private static final ConsistencyLevel getConfiguredConsistencyLevel() {
        String level = System.getenv("CASSANDRA_WRITE_CONSISTENCY_LEVEL");

        if (level == null) {
            return ConsistencyLevel.ALL;
        } else {
            return ConsistencyLevel.valueOf(level);
        }
    }

}
