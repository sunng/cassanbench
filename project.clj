(defproject cassanbench "0.1.0-SNAPSHOT"
  :description "FIXME: write description"
  :url "http://example.com/FIXME"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [[org.clojure/clojure "1.9.0"]
                 [com.datastax.cassandra/cassandra-driver-core "3.5.1"]
                 [info.sunng/ring-jetty9-adapter "0.11.3"]
                 [compojure "1.6.1"]
                 [org.slf4j/slf4j-log4j12 "1.7.25"]
                 [org.clojure/tools.logging "0.4.1"]]
  :main ^:skip-aot cassanbench.core
  :java-source-paths ["java/"]
  :target-path "target/%s"
  :profiles {:uberjar {:aot :all}}
  :jlink-modules ["java.base" "java.logging" "java.management" "java.desktop"]
  :plugins [[lein-jlink "0.2.0"]])
