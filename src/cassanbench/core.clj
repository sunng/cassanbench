(ns cassanbench.core
  (:gen-class)
  (:require [ring.adapter.jetty9 :refer [run-jetty]]
            [ring.util.response :as r]
            [compojure.core :refer :all]
            [clojure.tools.logging :as logging])
  (:import [cassanbench Bencher]
           [java.util UUID]))

(defn write [req]
  (try
    (Bencher/write (str (UUID/randomUUID)))
    (r/response "ok")

    (catch Exception e
      (logging/error "write error" e)
      (throw e))))

(defn write-all [req]
  (try
    (Bencher/writeAll (str (UUID/randomUUID)))
    (r/response "ok")

    (catch Exception e
      (logging/error "write error" e)
      (throw e))))

(defn write-one [req]
  (try
    (Bencher/writeOne (str (UUID/randomUUID)))
    (r/response "ok")

    (catch Exception e
      (logging/error "write error" e)
      (throw e))))

(defn write-any [req]
  (try
    (Bencher/writeAny (str (UUID/randomUUID)))
    (r/response "ok")

    (catch Exception e
      (logging/error "write error" e)
      (throw e))))

(defroutes app
  (GET "/write" [] write)
  (GET "/write-all" [] write-all)
  (GET "/write-one" [] write-one)
  (GET "/write-any" [] write-any))

(defn -main
  "I don't do a whole lot ... yet."
  [& args]
  (Bencher/init)
  (run-jetty app {:port 8080}))
